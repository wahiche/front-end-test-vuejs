###1. Login protected web app
-Use the GET users endpoint (https://jsonplaceholder.typicode.com/users)
-The “login” here is just entering the email address
-Ability to logout

###2. After logging in, display a dashboard view for the logged in user
1. A navigation bar across the top
2. Display the user’s name with hyperlink to website (Company’s name), example: Leanne Graham (Romaguera-Crona)
3. The navigation bar should have nav links to 3 different sections: Posts, Albums, and To Dos
####Posts
1. Display list of Posts (titles), click / expandable to display the body, display (count of comments) on the posts
2. Have a text search box: text search, display only posts that have matching full-word text either on title or body
####Albums
1. Display list of Albums (titles) and (count) of photos in the album
2. Ability to sort albums alphabetically by title
3. Link to “View Photos” which will pop-up a modal window that will display all the thumbnails of photos belong to that album
####To Dos
1. Display list of To Dos (titles) for the user
2. Ability to toggle: include / exclude completed
3. Completed task displayed with a checkbox, uncompleted task displayed with empty box

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## To view online

http://limelight.wahiche.com

##Login Creds

* Sincere@april.biz
* Shanna@melissa.tv
* Nathan@yesenia.net
* Julianne.OConner@kory.org
* Lucio_Hettinger@annie.ca
* Karley_Dach@jasper.info
* Telly.Hoeger@billy.biz
* Sherwood@rosamond.me
* Chaim_McDermott@dana.io
* Rey.Padberg@karina.biz

You cannot refresh the page when viewing it online there is some configuration on the server to rewrite some rules.




