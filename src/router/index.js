import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/Login'
import Dashboard from '@/pages/Dashboard'
import DashboardHome from '@/pages/DashboardHome'
import Posts from '@/pages/Posts'
import Albums from '@/pages/Albums'
import Todos from '@/pages/Todos'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Login
    },
    {
      path: '/dashboard',
      component: Dashboard,
      children: [
        {
          path: '',
          name: 'DashboardHome',
          component: Posts
        }
        ,
        {
          path: '/posts',
          name: 'Posts',
          component: Posts
        }
        ,
        {
          path: '/Albums',
          name: 'Albums',
          component: Albums
        }
        ,
        {
          path: '/todos',
          name: 'Todos',
          component: Todos
        }
      ]
    }
  ]
})
