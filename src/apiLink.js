import Vue from 'vue';

const sharedApiLink = {
    link: "https://jsonplaceholder.typicode.com/"
}

export default {
    sharedApiLink,
    install(Vue, options) {
        Vue.prototype.$baseApiUrl = sharedApiLink
    }
}